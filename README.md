# vs-rekrutacja-pzielinski

Cześć! 👋

W kilku miejscach dodałem pewne funkcjonalności (zwijanie sekcji "Zakres pomiaru" czy usuwanie tagów) w innych - zdecydowałem się ich nie wprowadzać (tak jak w przypadku selecta nad kategoriami produktów - spokojnie mógł się tam znaleźć pełnoprawny dropdown, ale uznałem, że nie będzie to miało większego sensu akurat przy tym tasku). 

Z bootstrapem miałem styczność pierwszy raz od dwóch lat ;) Ten projekt uświadomił mi, że bootstrapowy grid system jest w sumie przydatny i przy tego typu projektach może wpłynąć pozytywnie na proces wdrażania responsywności (co, jak mam w zwyczaju, zostawiłem sobie na sam koniec). Mam nadzieję na feedback odnośnie mojej "interpretacji" grid systemu, bo o ile responsywność udało mi się wprowadzić za pomocą bootstrapa i kilku media queries, tak nie wiem na ile poprawnie to zrobiłem. Bootstrapowe komponenty również starałem się stosować gdzie tylko było to możliwe, z wyłączeniem tagów/filtrów oraz wcześniej wspomnianego selecta.

🖥️ [Live Demo](https://mouflon.xyz/vs-task)

***

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
